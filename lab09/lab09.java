// Maddie Monahan
// Lab09
// Arrays
import java.util.Random;
import java.util.Scanner;

public class lab09{
  
public static void main(String[] args) {
 
  int array0[] = new int[8];

  
  for (int i = 0; i < 8; i++){
    array0[i] = (int)(Math.random()*20);
  }
  
 int[]array1 = copy(array0);
 int[]array2 = copy(array0);
  
  inverter(array0);
  print(array0);
  System.out.println(" ");
  inverter2(array1);
  print(array1);
  System.out.println(" ");
  int[]array3 = inverter2(array2);
  print(array3);
  System.out.println(" ");

  
}

  public static int[] copy(int[]x){
    
    int copiedArray[]= new int[8];
    for (int i = 0; i < 8; i++){
     copiedArray[i] = x[i];
    }
  
    return copiedArray;
    
  }
  
  public static void inverter(int[]y){
  
    int temp = 0;
    for (int i = 0; i < y.length / 2; i++){
      temp = y[y.length - i - 1];
      y[y.length - 1 - i] = y[i];
      y[i] = temp;
    }
   
  
  }
  
  public static int[] inverter2(int[]z){
  int[]flip = copy(z);
  inverter(flip);
  return flip;
  }
  
  public static void print(int[] a){
  
    for (int b = 0; b < a.length ; b++){
    System.out.print(a[b] + " ");
    }

 
     
  }
}
  