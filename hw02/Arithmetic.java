/////////////////
///CSE 2  Arithmetic Calculations
///Maddie Monahan
///2.2.18
//
public class Arithmetic {
  //main method required for every java program
  public static void main(String[]args) {
    
      //Number of pairs of pants
      int numPants = 3;
    
      //Cost per pair of pants
      double pantsPrice = 34.98;

      //Number of sweatshirts
      int numShirts = 2;
    
      //Cost per shirt
      double shirtPrice = 24.99;

      //Number of belts
      int numBelts = 1;
    
      //Cost per box of envelopes
      double beltCost = 33.99;

      //The tax rate in PA
      double paSalesTax = 0.06;
    
      //Assigns cost of pants to a variable and computes Cost of Pants 
      double totalCostOfPants = (numPants * pantsPrice); //Results total cost of Pants
      
      //Assigns cost of shirts to a variable and computes Cost of Shirts 
      double totalCostOfShirts = (numShirts * shirtPrice); //Results total cost of Shirts
    
      //Assigns cost of belts to a variable and computes Cost of Belts
      double totalCostOfBelts = (numBelts * beltCost); // Results total cost of Belts
    
      
      //Below code includes casting data from doubles to integers back to doubles
      //Purpose of this is to remove the extra decimals after a result
      //Ex: $3.199999 is not an accepted dollar value
      //Multiplying the 'total' number by 100 puts all necessary information into a form that can be casted as an integer
      //Uses the prefix 'convert' on variable of integer to show steps of casting
      //Converts data back to double to use as a correct dollar form '$xx.xx' by dividing by 100.0
      //This will give values below in correct form 'truncatedxy'
    
    
      //Calculate sales tax on items
    
      double taxonPants =  (totalCostOfPants * paSalesTax) * 100;
      int converttaxonPants = (int) taxonPants;
      double truncatedtaxonPants = (double) converttaxonPants / 100.0; 
      
      double taxonShirts = (totalCostOfShirts * paSalesTax) * 100; 
      int converttaxonShirts = (int) taxonShirts;
      double truncatedtaxonShirts = (double) converttaxonShirts / 100.0;
      
      double taxonBelts = (totalCostOfBelts * paSalesTax) * 100; 
      int converttaxonBelts = (int) taxonBelts;
      double truncatedtaxonBelts = (double) converttaxonBelts / 100.0;
     
      //Calculation of total Costs of all three items before tax
      double totalCostBeforeTax = (totalCostOfBelts + totalCostOfShirts + totalCostOfPants) * 100; 
      int convertCostBeforeTax = (int) totalCostBeforeTax;
      double truncatedtotalCostBeforeTax = (double) convertCostBeforeTax / 100.0;
    
      //Calculation for total sales tax
      double totalSalesTax = (truncatedtaxonBelts + truncatedtaxonShirts + truncatedtaxonPants) * 100;
      int converttotalSalesTax = (int) totalSalesTax;
      double truncatedtotalSalesTax = (double) converttotalSalesTax / 100.0;
    
      //Final cost is the total cost before tax minus the total sales tax of the items
      double finalCost = (truncatedtotalCostBeforeTax + truncatedtotalSalesTax) * 100;
      int convertfinalCost = (int) finalCost;
      double truncatedfinalCost = (double) convertfinalCost / 100.0;
      
      //Prints out all data necessary to complete HW02
      System.out.println ("Total Cost of Pants: $" + totalCostOfPants);
      System.out.println ("Total Cost of Shirts: $" + totalCostOfShirts);
      System.out.println ("Total Cost of Belts: $" + totalCostOfBelts);
      System.out.println ("Tax on Pants: $" + truncatedtaxonPants);
      System.out.println ("Tax on Shirts: $" + truncatedtaxonShirts);
      System.out.println ("Tax on Belts: $" + truncatedtaxonBelts);
      System.out.println ("Total Cost Before Tax: $" + truncatedtotalCostBeforeTax);
      System.out.println ("Total Sales Tax: $" + String.format("%.2f",truncatedtotalSalesTax)); //uses string format to keep 0 in final decimal place
    
      }
  //end of main method
}
//end of class
    
      
    
      
    
      
      
    