/////////////////
///CSE 2 HW04
///Yahtzee
//Maddie Monahan
//2.18.18
//
import java.util.Scanner; //imports Scanner to ask for data in program

//introduce the specific class for lab, should match the name of program
public class Yahtzee{
  
    		// main method required for every Java program
   			public static void main(String[] args) {
        System.out.println("Would you like to play this game randomly or would you like to input your own roll values?");
        
        //input scanner to ask random or input own rolls
        Scanner myScanner = new Scanner (System.in);
        System.out.print("Type '1' to do random or '2' to manually input: ");
          
        //cast game selection as in integer
        int gameSelection = myScanner.nextInt();
        
        //if you select random, start game
        if (gameSelection == 1){
          
            //randomize five rolls from [1,6]
            int roll1 = (int)(Math.random()*6)+1;
            int roll2 = (int)(Math.random()*6)+1;
            int roll3 = (int)(Math.random()*6)+1;
            int roll4 = (int)(Math.random()*6)+1;
            int roll5 = (int)(Math.random()*6)+1;
          
            //Print your rolls
            System.out.println("Your roll is: " + roll1 + roll2 + roll3 + roll4 + roll5);
            
            //start counters for scoring yahtzee card
            int countselectOne = 0;
            int countselectTwo = 0;
            int countselectThree = 0;
            int countselectFour = 0;
            int countselectFive = 0;
            int countselectSix = 0;
              
            //begin upper score for Aces
            if (roll1 == 1) {
              countselectOne++;
            }
            if (roll2 == 1){
              countselectOne++;
            }
            if(roll3 == 1){
              countselectOne++;
            }
            if(roll4 == 1){
              countselectOne++;
            }
            if(roll5 == 1){
              countselectOne++;
            }
          
            //print aces score
            System.out.println("Aces score is: " + countselectOne);
            
            //begin score for twos
            if (roll1 == 2){
              countselectTwo++;
            }
            if (roll2 == 2){
              countselectTwo++;
            }
            if (roll3 == 2){
              countselectTwo++;
            }
            if (roll4 == 2){
              countselectTwo++;
            }
            if (roll5 == 2){
              countselectTwo++;
            }
            
            //print twos score
            System.out.println("Twos score is: " + countselectTwo*2);
          
            //start scoring for 3s
            if (roll1 == 3){
              countselectThree++;
            }
            if (roll2 == 3){
              countselectThree++;
            }
            if (roll3 == 3){
              countselectThree++;
            }
            if (roll4 == 3){
              countselectThree++;
            }
            if (roll5 == 3){
              countselectThree++;
            }
            
            //print threes score
            System.out.println("Threes score is: " + countselectThree*3);
          
            //start scoring for fours
            if (roll1 == 4){
              countselectFour++;
            }
            if (roll2 == 4){
              countselectFour++;
            }
            if (roll3 == 4){
              countselectFour++;
            }
            if (roll4 == 4){
              countselectFour++;
            }
            if (roll5 == 4){
              countselectFour++;
            }
            
            //print score for fours
            System.out.println("Fours score is: " + countselectFour*4);
          
            //start scoring for fives
            if (roll1 == 5){
              countselectFive++;
            }
            if (roll2 == 5){
              countselectFive++;
            }
            if (roll3 == 5){
              countselectFive++;
            }
            if (roll4 == 5){
              countselectFive++;
            }
            if (roll5 == 5){
              countselectFive++;
            }
          
            //print fives score
            System.out.println("Fives score is: " + countselectFive*5);
          
            //start score for sixes
            if (roll1 == 6){
              countselectSix++;
            }
            if (roll2 == 6){
              countselectSix++;
            }
            if (roll3 == 6){
              countselectSix++;
            }
            if (roll4 == 6){
              countselectSix++;
            }
            if (roll5 == 6){
              countselectSix++;
            }
          
            //prints sixes score
            System.out.println("Sixes score is: " + countselectSix*6);
          
            //upper section score is how many sixes * six, fives * fives, etc.
            int totalUpperSectionSelect = countselectSix*6+countselectFive*5+countselectFour*4+countselectThree*3+countselectTwo*2+countselectOne;
          
            //print score for upper section
            System.out.println("Total score for Upper Section is: " + totalUpperSectionSelect);
          
            //bonus for upper section
            int bonusSelect = 0;
            if (totalUpperSectionSelect > 63){
              bonusSelect = 35;
            }
            else{
              bonusSelect = 0;
            }
            System.out.println("Bonus: " + bonusSelect);
          
            //Totals of upper half = the upper score and the bonus of 35 if score is over 63
            int uppersectionSelectwithBonus = bonusSelect + totalUpperSectionSelect;
          
            //print total upper section toal
            System.out.println("Upper Section Total: " + uppersectionSelectwithBonus);
        
            
          
            //lower section start 
            //start three of a kind counter for full house
            int threeofakind = 0;
          
            //start three of a kind solution for scoring counter
            int threeofakindSolution1 = 0;
            
            if (countselectOne == 3){
              threeofakind++;
              threeofakindSolution1 = 3;
            }
            if (countselectTwo == 3){
              threeofakind++;
              threeofakindSolution1 = 6;
            }
            if (countselectThree == 3){
              threeofakind++;
              threeofakindSolution1 = 9;
            }
            if (countselectFour == 3){
              threeofakind++;
              threeofakindSolution1 = 12;
            }
            if (countselectFive == 3){
              threeofakind++;
              threeofakindSolution1 = 15;
            }
            if (countselectSix == 3){
              threeofakind++;
              threeofakindSolution1 = 18;
            }
            if (countselectOne != 3 && countselectTwo != 3 && countselectThree != 3 && countselectFour != 3 && countselectFive != 3 && countselectSix != 3){
              threeofakindSolution1 = 0;
            }
            //Print out three of a kind score
            System.out.println("Three of a Kind Score: " + threeofakindSolution1);
            
            //start four of a kind
            
            //set four of a kind score counter to zero
            int fourofakindSolution1 = 0;
            
            if (countselectOne == 4){
              fourofakindSolution1 = 4;
            }
            if (countselectTwo == 4){
              fourofakindSolution1 = 8;
            }
            if (countselectThree == 4){
              fourofakindSolution1 = 12;
            }
            if (countselectFour == 4){
              fourofakindSolution1 = 16;
            }
            if (countselectFive == 4){
              fourofakindSolution1 = 20;
            }
            if (countselectSix == 4){
              fourofakindSolution1 = 24;
            }
            if (countselectOne != 4 && countselectTwo != 4 && countselectThree != 4 && countselectFour != 4 && countselectFive != 4 && countselectSix != 4){
             fourofakindSolution1 = 0;
            }
            
            //Print four of a kind solution
            System.out.println("Four of a Kind Score: " + fourofakindSolution1);
              
     
            //start full house
            //initialize matchingDice counter to 0
            int matchingDice1 = 0;
            if (countselectOne == 2){
              matchingDice1++;
            }
            if (countselectTwo == 2){
              matchingDice1++;
            }
            if (countselectThree == 2){
              matchingDice1++;
            }
            if (countselectFour == 2){
              matchingDice1++;
            }
            if (countselectFive == 2){
              matchingDice1++;
            }
            if (countselectSix == 2){
              matchingDice1++;
            }
            
            //initialize full house Solution score to 0
            int fullhouseSolution1 = 0;
            
            //full house means two matching dice and three of a kind
            if (matchingDice1 == 1 && threeofakind == 1){
              fullhouseSolution1 = 25;
            }
            else{
              fullhouseSolution1 = 0;
            }
          
            //print full house score
            System.out.println("Full House Score: " + fullhouseSolution1);
            
            //start Small Straight calculation
            int smallstraightSolution1 = 0;
            if((countselectOne > 0 && countselectTwo > 0 & countselectThree > 0 && countselectFour > 0) || (countselectTwo > 0 && countselectThree > 0 && countselectFour > 0 && countselectFive > 0) || (countselectThree > 0 && countselectFour > 0 && countselectFive > 0 && countselectSix > 0)){
              smallstraightSolution1 = 30;
            }
            else{
              smallstraightSolution1 = 0;
            }
          
            //print small straight solution
            System.out.println("Small Straight Score: " + smallstraightSolution1);
            
            //start Large Straight calculation
            int largestraightSolution1 = 0;
            if((countselectOne > 0 && countselectTwo > 0 && countselectThree > 0 && countselectFour > 0 && countselectFive > 0) || (countselectTwo > 0 && countselectThree > 0 && countselectFour > 0 && countselectFive > 0 && countselectSix > 0)){
              largestraightSolution1 = 40;
            }
            else{
              largestraightSolution1 = 0;
            }
            //print large straight calculation
            System.out.println("Large Straight Score: " + largestraightSolution1);
            
            //start yahtzee calculation: if all five values are the same, yahtzee
            int yahtzeeSolution1 = 0;
            if (countselectOne == 5 || countselectTwo == 5 || countselectThree == 5 || countselectFour == 5 || countselectSix == 5){
              yahtzeeSolution1 = 50;
            }
            else{
              yahtzeeSolution1 = 0;
            }
          
            //print yahtzee solution
            System.out.println("Yahtzee: " + yahtzeeSolution1);
          
            //chance
            int chanceSelect= roll1 + roll2 + roll3 + roll4 + roll5;
            System.out.println("Chance score:" + chanceSelect);
          
            int totalLowerSectionSel = threeofakindSolution1 + fourofakindSolution1 + fullhouseSolution1 + smallstraightSolution1 + largestraightSolution1 + yahtzeeSolution1 + chanceSelect;
            System.out.println("Total Lower Section: " + totalLowerSectionSel);
            System.out.println("Upper Section: " + totalUpperSectionSelect);
            int GrandTotalSelect= totalUpperSectionSelect + totalLowerSectionSel;
            System.out.println("Grand Total: " + GrandTotalSelect);
         }
         
        //second option of game, manual input
        else if (gameSelection == 2){
          
          //create new scanner to read new five digit integer
          Scanner maninputScanner = new Scanner (System.in);
          System.out.print("Input your dice rolls as a five digit number: ");
          
          //cast input as an integer
          int inputRoll = myScanner.nextInt();
          
          //these steps seperate the five digit integer into seperate, singular integer values
          int fifthRoll= inputRoll / 10000;
          int remFour = inputRoll % 10000;
          int fourthRoll = remFour / 1000;
          int remThree = remFour % 1000;
          int thirdRoll = remThree / 100;
          int remTwo = remThree % 100;
          int secondRoll = remTwo / 10;
          int remOne = remTwo % 10;
          int firstRoll = remOne / 10;
          int remFinal = remOne / 1;
          int finalRoll = remFinal;
          
          //print statement for incorrect data input
          if ((fifthRoll > 6 || fifthRoll < 1) || (fourthRoll > 6 || fourthRoll < 1) || (thirdRoll > 6 || thirdRoll < 1) || (secondRoll > 6 || secondRoll < 1) || (finalRoll > 6 || finalRoll < 1) || (inputRoll > 66666)){
            System.out.println("You input a dice value that is incorrect. Dice are from [1,6] and you have only five rolls.");
          }
          
          //if inputs are correct, initialize counters for dice 
          else {
            int countOne = 0;
            int countTwo = 0;
            int countThree = 0;
            int countFour = 0;
            int countFive = 0;
            int countSix = 0;
              
            //begin upper score for Aces
            // ex. if there is a roll with a one in it in the first digit, add one to counter
            if (finalRoll == 1){
              countOne++;
            }
            if (secondRoll == 1){
              countOne++;
            }
            if(thirdRoll == 1){
              countOne++;
            }
            if(fourthRoll == 1){
              countOne++;
            }
            if(fifthRoll == 1){
              countOne++;
            }
            //print out aces score
            System.out.println("Aces score is: " + countOne);
            
            //begin scoring for Twos
            if (finalRoll == 2){
              countTwo++;
            }
            if (secondRoll == 2){
              countTwo++;
            }
            if (thirdRoll == 2){
              countTwo++;
            }
            if (fourthRoll == 2){
              countTwo++;
            }
            if (fifthRoll == 2){
              countTwo++;
            }
            //Print out twos score
            System.out.println("Twos score is: " + countTwo*2);
            
            //begin scoring for Threes
            if (finalRoll == 3){
              countThree++;
            }
            if (secondRoll == 3){
              countThree++;
            }
            if (thirdRoll == 3){
              countThree++;
            }
            if (fourthRoll == 3){
              countThree++;
            }
            if (fifthRoll == 3){
              countThree++;
            }
            //print score for threes
            System.out.println("Threes score is: " + countThree*3);
            
            //start scoring for fours
            if (finalRoll == 4){
              countFour++;
            }
            if (secondRoll == 4){
              countFour++;
            }
            if (thirdRoll == 4){
              countFour++;
            }
            if (fourthRoll == 4){
              countFour++;
            }
            if (fifthRoll == 4){
              countFour++;
            }
            //print score for fours
            System.out.println("Fours score is: " + countFour*4);
            
            //start scoring for fives
            if (finalRoll == 5){
              countFive++;
            }
            if (secondRoll == 5){
              countFive++;
            }
            if (thirdRoll == 5){
              countFive++;
            }
            if (fourthRoll == 5){
              countFive++;
            }
            if (fifthRoll == 5){
              countFive++;
            }
            //print fives score
            System.out.println("Fives score is: " + countFive*5);
            
            //start sixes score
            if (finalRoll == 6){
              countSix++;
            }
            if (secondRoll == 6){
              countSix++;
            }
            if (thirdRoll == 6){
              countSix++;
            }
            if (fourthRoll == 6){
              countSix++;
            }
            if (fifthRoll == 6){
              countSix++;
            }
            //print sixes score
            System.out.println("Sixes score is: " + countSix*6);
            
            //calculation of upper section score
            int totalUpperSection = countSix*6+countFive*5+countFour*4+countThree*3+countTwo*2+countOne;
            System.out.println("Score for Upper Section: " + totalUpperSection);
            
            //bonus calculation: if score is over 63 in top half, add 35 to final
            int bonusManual = 0;
            if (totalUpperSection > 63){
              bonusManual = 35;
            }
            else{
              bonusManual = 0;
            }
            //print bonus score, if applicable
            System.out.println("Bonus:" + bonusManual);
            
            //Totals of upper half = the upper score and the bonus of 35 if score is over 63
            int uppersectionwithBonus = bonusManual + totalUpperSection;
            //print 
            System.out.println("Upper Section Total: " + uppersectionwithBonus);
            
            
            //begin lowersection
            
            
            //start three of a kind
            
            //start three of a kind counter -Man- to 0 for full house calculation
            //start three of a kind counter -solution- for scoring
            int threeofakindManual = 0;
            int threeofakindSolution = 0;
            
            if (countOne == 3){
              threeofakindManual++;
              threeofakindSolution = 3;
            }
            if (countTwo == 3){
              threeofakindManual++;
              threeofakindSolution = 6;
            }
            if (countThree == 3){
              threeofakindManual++;
              threeofakindSolution = 9;
            }
            if (countFour == 3){
              threeofakindManual++;
              threeofakindSolution = 12;
            }
            if (countFive == 3){
              threeofakindManual++;
              threeofakindSolution = 15;
            }
            if (countSix == 3){
              threeofakindManual++;
              threeofakindSolution = 18;
            }
            if (countOne != 3 && countTwo != 3 && countThree != 3 && countFour != 3 && countFive != 3 && countSix != 3){
              threeofakindSolution = 0;
            }
            //Print out three of a kind score
            System.out.println("Three of a Kind Score: " + threeofakindSolution);
            
            //start four of a kind
            
            //set four of a kind score counter to zero
            int fourofakindSolution = 0;
            
            if (countOne == 4){
              fourofakindSolution = 4;
            }
            if (countTwo == 4){
              fourofakindSolution = 8;
            }
            if (countThree == 4){
              fourofakindSolution = 12;
            }
            if (countFour == 4){
              fourofakindSolution = 16;
            }
            if (countFive == 4){
              fourofakindSolution = 20;
            }
            if (countSix == 4){
              fourofakindSolution = 24;
            }
            if (countOne != 4 && countTwo != 4 && countThree != 4 && countFour != 4 && countFive != 4 && countSix != 4){
             fourofakindSolution = 0;
            }
            //Print four of a kind solution
            System.out.println("Four of a Kind Score: " + fourofakindSolution);
              
     
            //start full house
            //initialize matchingDice counter to 0
            int matchingDice = 0;
            if (countOne == 2){
              matchingDice++;
            }
            if (countTwo == 2){
              matchingDice++;
            }
            if (countThree == 2){
              matchingDice++;
            }
            if (countFour == 2){
              matchingDice++;
            }
            if (countFive == 2){
              matchingDice++;
            }
            if (countSix == 2){
              matchingDice++;
            }
            
            //initialize full house Solution score to 0
            int fullhouseSolution = 0;
            
            //full house means two matching dice and three of a kind
            if (matchingDice == 1 && threeofakindManual == 1){
              fullhouseSolution = 25;
            }
            else{
              fullhouseSolution = 0;
            }
            //print full house score
            System.out.println("Full House Score: " + fullhouseSolution);
            
            //start Small Straight calculation
            int smallstraightSolution = 0;
            if((countOne > 0 && countTwo > 0 & countThree > 0 && countFour > 0) || (countTwo > 0 && countThree > 0 && countFour > 0 && countFive > 0) || (countThree > 0 && countFour > 0 && countFive > 0 && countSix > 0)){
              smallstraightSolution = 30;
            }
            else{
              smallstraightSolution = 0;
            }
            //print small straight solution
            System.out.println("Small Straight Score: " + smallstraightSolution);
            
            //start Large Straight calculation
            int largestraightSolution = 0;
            if((countOne > 0 && countTwo > 0 && countThree > 0 && countFour > 0 && countFive > 0) || (countTwo > 0 && countThree > 0 && countFour > 0 && countFive > 0 && countSix > 0)){
              largestraightSolution = 40;
            }
            else{
              largestraightSolution = 0;
            }
            //print large straight calculation
            System.out.println("Large Straight Score: " + largestraightSolution);
            
            //start yahtzee calculation: if all five values are the same, yahtzee
            int yahtzeeSolution = 0;
            if (countOne == 5 || countTwo == 5 || countThree == 5 || countFour == 5 || countSix == 5){
              yahtzeeSolution = 50;
            }
            else{
              yahtzeeSolution = 0;
            }
            //print yahtzee solution
            System.out.println("Yahtzee: " + yahtzeeSolution);
            
            
            //Start chance calculations: add all rolls together
            int chance = finalRoll + secondRoll + thirdRoll + fourthRoll + fifthRoll;
            System.out.println("Chance score: " + chance);
            
            //totalslowersection
           
            int totalLowerSectionMan = threeofakindSolution + fourofakindSolution + fullhouseSolution + smallstraightSolution + largestraightSolution + yahtzeeSolution + chance;
            System.out.println("Total Lower Section: " + totalLowerSectionMan);
            System.out.println("Upper Section: " + totalUpperSection);
            int GrandTotalManual = totalUpperSection + totalLowerSectionMan;
            System.out.println("Grand Total: " + GrandTotalManual);
   
            }
            }
            }
            }
