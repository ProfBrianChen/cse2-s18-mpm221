/////////////////
///CSE 2  Convert Homework
///Maddie Monahan
// HW03
///2.13.18
//
import java.util.Scanner; //imports scanner to ask for data in program 
public class Convert { //introduce class for program
  //main method required for every java program
  public static void main(String[]args) { 
    Scanner myScanner = new Scanner (System.in); //constructs instance of scanner to receive input
    System.out.print("Enter the affected area in acres: "); //asks for input on affected area
    double acrearea = myScanner.nextDouble(); //casts input from scanner as a double called acre area
    System.out.print("Enter the rainfall in the affected area: "); //asks for input in inches of rainfall
    double rainfall = myScanner.nextDouble(); //uses data from scanner and casts as a double called rainfall
    double ConversiontoGallons = (acrearea * rainfall) * 27154.285990761; //converts data to gallons
    double ConversiontoMiles = ConversiontoGallons * 9.08169e-13; //converts data from gallons to cubic miles
    System.out.println(ConversiontoMiles + " cubic miles"); //prints final computation
  } //end of main method
} //end of class
    
    
    
    
    
