/////////////////
///CSE 2  Pyramid Homework
///Maddie Monahan
// HW03
///2.13.18
//

import java.util.Scanner; //imports scanner to ask for data in program 
public class Pyramid { //introduce class for program, Pyramid
  //main method required for every java program
  public static void main(String[]args) {
    Scanner myScanner = new Scanner (System.in); //constructs instance of scanner to receive inputs  
    System.out.print("The square side of the pyramid is (input length): "); //asks for side length
    double squareside = myScanner.nextDouble(); //declares previous input as a double called squareside
    System.out.print("The height of the pyramid is (input height): "); //asks for height of pyramid
    double height = myScanner.nextDouble(); //declares previous input as a double called height
    double basearea = (squareside * squareside); //computes base area of pyramid for volume calculation
    double volume = (basearea*height)/3; //computes volume of pyramid
    System.out.println("The volume inside the pyramid is: " + volume ); //prints final statement regarding volume of pyramid
  }//end of main method
}//end of class