/////////////////
///CSE 2 Hello Class
///
public class WelcomeClass {
  //main method required for every java program
  public static void main(String[]args) {
    // Prints Paragraph in Window
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println(" ^  ^  ^  ^  ^  ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-M--P--M--2--2--1->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /"); //double backslash because if used one would be interpreted as symbol
    System.out.println("v  v  v  v  v  v");
    System.out.println("My name is Maddie and I am from Fairfield, CT. I am also on the field hockey team at Lehigh.");
   }
  //end of main method
}
//end of class