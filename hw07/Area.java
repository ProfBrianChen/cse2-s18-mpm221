// Maddie Monahan
// HW 07
// Area of Different Shapes
//
import java.util.Scanner; //imports Scanner to get input

public class Area{//initializes class
    //sets up first method for rectangle
    public static double rectangleArea(double rectangleHeight, double rectangleWidth){
      //initializes equation for rectangle area, calls that method
      double rectangleArea = rectangleHeight*rectangleWidth;
      //prints area
      System.out.println("The area is: " + rectangleArea);
      //returns value
      return rectangleArea;
    }
    //sets up second method, follows same pattern for diff shape
    public static double triangleArea(double triangleHeight, double triangleBase){
      //calls triangle area method
      double triangleArea = 0.5 * triangleBase * triangleHeight;
      System.out.println("The area is: " + triangleArea);
      return triangleArea;
    }
    //sets up third method, follows same pattern for diff shape
    public static double circleArea(double circleRadius){
      //calling on circlearea method
      double circleArea = Math.PI * circleRadius * circleRadius;
      System.out.println("The area is: " + circleArea);
      return circleArea;
    }
   
    //initializes main method
    public static void main (String[]args){
      Scanner myScanner = new Scanner(System.in);
      System.out.print("select 'rectangle', 'triangle' or 'circle' to compute area of: ");
      String rectangle = "rectangle";
      String triangle = "triangle";
      String circle = "circle";
      String selection = myScanner.next();
      //ensures user is inputting a correct shape
      while((!selection.equals(rectangle))&& (!selection.equals(triangle))&& (!selection.equals(circle))){
            System.out.print("Incorrect input. Must be one of the previously stated three shapes, with no capital letters: ");
            selection = myScanner.next();
      }
      //ensuring data is validated as doulbes for rectangle
      while(selection.equals(rectangle)){
        System.out.print("enter rectangle height: ");
         while(!myScanner.hasNextDouble()){
            System.out.print("Error, input a double: ");
            String invalid = myScanner.next();
      
         }
         double rectangleHeight = myScanner.nextDouble();
         System.out.print("enter rectangle width: ");
         while(!myScanner.hasNextDouble()){
           System.out.print(" ");
            String invalid = myScanner.next();
         }
        double rectangleWidth = myScanner.nextDouble();
        rectangleArea(rectangleHeight, rectangleWidth);
        break;
      }
      //ensuring data is validated as doulbes for triangle
      while(selection.equals(triangle)){
        System.out.print("enter triangle height: ");
        while(!myScanner.hasNextDouble()){
            System.out.print("Error, input a double: ");
            String invalid = myScanner.next();
         }
        double triangleHeight = myScanner.nextDouble();
        System.out.print("enter triangle base length: ");
        while(!myScanner.hasNextDouble()){
            System.out.print("Input a double: ");
            String invalid = myScanner.next();
         }
        double triangleBase = myScanner.nextDouble();
        triangleArea(triangleHeight, triangleBase);
        break;
      }
      //ensuring data is validated as a doulbe for circle
      while(selection.equals(circle)){
        System.out.print("enter radius: ");
        while(!myScanner.hasNextDouble()){
            System.out.print("Error, input a double: ");
            String invalid = myScanner.next();
         }
        double circleRadius = myScanner.nextDouble();
        circleArea(circleRadius);
        break;
    }//end of while loop
  }//end of method
}//end of class