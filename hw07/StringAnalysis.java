//Maddie Monahan
//HW07 part 2
//String Analysis - checking if strings are letters
//
import java.util.Scanner; //imports scanner

public class StringAnalysis{ //initializes class

public static void main (String[]args){ //initializes main method
  //intializes scanner called myScanner
  Scanner myScanner = new Scanner(System.in);
  //Asks for input
  System.out.print("Enter your string: ");
  //casts input to a string
  String m = myScanner.next();
  //asks second question, to part methods
  System.out.print("Do you want to analyze the entire string? 1 for yes 2 for no :  ");
  //cast input to integer 
  int input = myScanner.nextInt();
  if(input == 1){
    stringAnalysis(m);
  }
  else if (input == 2){
  //second half of program, manual input
  System.out.print("You are now analyzing a specific portion of your chosing of a String. Enter your string again to begin this process: ");
  String p = myScanner.next(); //sets second string to p for second validation
  //ensure data is input
  while(true){
  System.out.print("Enter quantity you want to analyze: ");
  while(!myScanner.hasNextInt()){
      System.out.print("Input an int: ");
      String invalid = myScanner.next();
  }
  break;
  }
  int quantEval = myScanner.nextInt();
  //hits second method
  stringAnalysis2(p, quantEval);
  }//end of if 
}//end of method
  public static boolean stringAnalysis (String z){ //start of second method
    int i = 0;
    int count = z.length(); //initializes length of z string
    if(Character.isLetter(z.charAt(i))){
      if(i<= count){
        i++; //adds one to loop of count 
        System.out.println("All letters!"); //correct input result
      }
    }
   else{
     System.out.println("Not all are letters in this string. "); //returns incorrect message
   }
    return true; //returns true boolean
  }
  
  public static boolean stringAnalysis2 (String x, int quantEval){ //third method for validation of x
    int j = 0;
    int secondCount = x.length();
    if (j >= quantEval){
      if(Character.isLetter(x.charAt(j))){
      j++;
      System.out.println("All are letters.");
    }
  }
    else{
      System.out.println("Not all of the characters in the string are letters.");
  }
  return true; // boolean complete
}//end of method
}//end of class