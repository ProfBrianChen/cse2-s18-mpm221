// Maddie Monahan
// HW09
// Poker

import java.util.Scanner; // import scanner
import java.util.Arrays; // imports Array class

public class DrawPoker{ // initializes class
  
  public static void main(String [] arg){ // begin main method
 
  int deck[]= new int[52]; // initialize array for deck of 52 cards
 
  for (int index = 0; index < deck.length; index++){ // for loop to distribute cards
    deck[index]=index;
  } // make deck
    
  for (int i = 0; i < 52; i++){ // shuffle deck
    int random = i + (int)(Math.random()*(52-i));
    int temp = deck[random];
    deck[random] = deck[i];
    deck[i] = temp;
  } // end of shuffling
  
  int player1[] = new int[5]; // initialize player 1 card hand
  int player2[] = new int[5]; // initialize player 2 card hand
  
   // deals deck across into each players hands
   player1[0] = deck[0];
   player2[0] = deck[1];
   player1[1] = deck[2];
   player2[1] = deck[3];
   player1[2] = deck[4];
   player2[2] = deck[5];
   player1[3] = deck[6];
   player2[3] = deck[7];
   player1[4] = deck[8];
   player2[4] = deck[9];
    
  String dealthandplayer1[] = new String[5]; // initialize player 1 card hand
  String dealthandplayer2[] = new String[5]; // initialize player 2 card hand

    dealthandplayer1 = card(player1); // passing to card method to get string card names
    dealthandplayer2 = card(player2); // passing to card method to get string card names
   
   System.out.println("     ");
   System.out.println("Player One's Cards:");
   System.out.println("     ");
    
    for (int i = 0; i < 5; i++){ // for loop to print player 1 cards
      System.out.println(dealthandplayer1[i]);
    } 
   
    System.out.println("     ");
    System.out.println("Player Two's Cards:");
    System.out.println("     ");
    
    for (int j = 0; j < 5; j++){ // for loop to print player 2 cards
      System.out.println(dealthandplayer2[j]);
    }
    System.out.println("     ");
    String W = ""; 
    
    if (flush(player1) != flush(player2)){
      if (flush(player1) == true){
        W = "P1"; 
       }
    else if (flush(player2) == true){
        W = "P2";
      }
    }
    
    if (fullhouse(player1) != fullhouse(player2)){
      if (fullhouse(player1) == true){
        W = "P1"; 
      }
      else if (fullhouse(player2) == true){
        W = "P2";
      }
    }
   
    if (three(player1) != three(player2)){
      if (three(player1) == true){
        W = "P1"; 
      }
      else if (three(player2) == true){
        W = "P2";
      }
    }
    
    
    if (pair(player1) != pair(player2)){
      if (pair(player1) == true){
        W = "P1"; 
      }
    else if (pair(player2) == true){
       W = "P2";
      }
    }
 
    if(pair(player1) == false && pair(player2) == false){
      if(flush(player1) ==false && flush(player2) == false){
        if(three(player1) == false && three(player2) == false){
          if(fullhouse(player1) == false && fullhouse(player2) == false){
              int highp1 = highcardhand(player1); 
              int highp2 = highcardhand(player2); 
              if (highp1 > highp2){
                 System.out.println("The winner of this game is player 1 due to high card hand");
               }
              else if (highp2 > highp1){
                System.out.println("the winner of this game is player 2 due to high card hand");
              } 
              else {
              System.out.println("Tie, same high card hand.");
              }
          }
        }
      }
    }
    else{
      System.out.println("The winner of this game is: " + W);
    }
} // end of main method
      
   public static String [] card(int [] dealt){ // string method for cars
    String [] card = new String [5]; // initialize string array length of 5 
    for (int i = 0; i < card.length; i++){ // for 5 cards, figure out the stringed equivalent
      
      if (dealt[i] % 13 == 0){ //aces
        if(dealt[i]/4 == 1){
        card [i] = "Ace of Diamonds";
        }
        else if(dealt[i]/4 == 2){
        card [i] = "Ace of Clubs";
        }
        else if(dealt[i]/4 == 3){
        card [i] = "Ace of Hearts";
        }
        else{
        card [i] = "Ace of Spades";
        }
      }
      if (dealt[i] % 13 == 1){ //twos
        if(dealt[i]/4 == 1){
        card [i] = "2 of Diamonds";
        }
        else if(dealt[i]/4 == 2){
        card [i] = "2 of Clubs";
        }
        else if(dealt[i]/4 == 3){
        card [i] = "2 of Hearts";
        }
        else{
        card [i] = "2 of Spades";
        }
      }
      if (dealt[i] % 13 == 2){ //threes
        if(dealt[i]/4 == 1){
        card [i] = "3 of Diamonds";
        }
        else if(dealt[i]/4 == 2){
        card [i] = "3 of Clubs";
        }
        else if(dealt[i]/4 == 3){
        card [i] = "3 of Hearts";
        }
        else{
        card [i] = "3 of Spades";
        }
      }
      if (dealt[i]% 13 == 3){ //fours
        if(dealt[i]/4 == 1){ 
        card [i] = "4 of Diamonds";
        }
        else if(dealt[i]/4 == 2){
        card [i] = "4 of Clubs";
        }
        else if(dealt[i]/4 == 3){
        card [i] = "4 of Hearts";
        }
        else{
        card [i] = "4 of Spades";
        }
      }
      if (dealt[i] % 13 == 4){ //fives
        if(dealt[i]/4 == 1){
        card [i] = "5 of Diamonds";
        }
        else if(dealt[i]/4 == 2){
        card [i] = "5 of Clubs";
        }
        else if(dealt[i]/4 == 3){
        card [i] = "5 of Hearts";
        }
        else{
        card [i] = "5 of Spades";
        }
      }         
      if (dealt[i]% 13 == 5){ //sixes
        if(dealt[i]/4 == 1){
        card [i] = "6 of Diamonds";
        }
        else if(dealt[i]/4 == 2){
        card [i] = "6 of Clubs";
        }
        else if(dealt[i]/4 == 3){
        card [i] = "6 of Hearts";
        }
        else{
        card [i] = "6 of Spades";
        }
      }         
      if (dealt[i]% 13 == 6){ // sevens
        if(dealt[i]/4 == 1){
        card [i] = "7 of Diamonds";
        }
        else if(dealt[i]/4 == 2){
        card [i] = "7 of Clubs";
        }
        else if(dealt[i]/4 == 3){
        card [i] = "7 of Hearts";
        }
        else{
        card [i] = "7 of Spades";
        }
      }         
      if (dealt[i]% 13 == 7){ //eights
        if(dealt[i]/4 == 1){
        card [i] = "8 of Diamonds";
        }
        else if(dealt[i]/4 == 2){
        card [i] = "8 of Clubs";
        }
        else if(dealt[i]/4 == 3){
        card [i] = "8 of Hearts";
        }
        else{
        card [i] = "8 of Spades";
        }
      }         
      if (dealt[i]% 13 == 8){ //nines
        if(dealt[i]/4 == 1){
        card [i] = "9 of Diamonds";
        }
        else if(dealt[i]/4 == 2){
        card [i] = "9 of Clubs";
        }
        else if(dealt[i]/4 == 3){
        card [i] = "9 of Hearts";
        }
        else{
        card [i] = "9 of Spades";
        }
      }         
      if (dealt[i]% 13 == 9){ //tens
        if(dealt[i]/4 == 1){
        card [i] = "10 of Diamonds";
        }
        else if(dealt[i]/4 == 2){
        card [i] = "10 of Clubs";
        }
        else if(dealt[i]/4 == 3){
        card [i] = "10 of Hearts";
        }
        else{
        card [i] = "10 of Spades";
        }
      }         
      if (dealt[i]% 13 == 10){ // jacks
        if(dealt[i]/4 == 1){
        card [i] = "Jack of Diamonds";
        }
        else if(dealt[i]/4 == 2){
        card [i] = "Jack of Clubs";
        }
        else if(dealt[i]/4 == 3){
        card [i] = "Jack of Hearts";
        }
        else{
        card [i] = "Jack of Spades";
        }
      }         
      if (dealt[i]% 13 == 11){ //queen
        if(dealt[i]/4 == 1){
        card [i] = "Queen of Diamonds";
        }
        else if(dealt[i]/4 == 2){
        card [i] = "Queen of Clubs";
        }
        else if(dealt[i]/4 == 3){
        card [i] = "Queen of Hearts";
        }
        else{
        card [i] = "Queen of Spades";
        }
      }
     if (dealt[i]% 13 == 12){ //kings
        if(dealt[i]/4 == 1){
        card [i] = "King of Diamonds";
        }
        else if(dealt[i]/4 == 2){
        card [i] = "King of Clubs";
        }
        else if(dealt[i]/4 == 3){
        card [i] = "King of Hearts";
        }
        else{
        card [i] = "King of Spades";
        }
      } 
      
    } // end of for loop
         
    return card; 
    
    }//end of card method

  
  public static boolean pair(int[]pokerhand){ //initialize pair method
    boolean pairBoolean = false; // initialize boolean 
    int counter = 0; // initialzie counter
     // if loops are all possible pairs 
     if  (pokerhand[0] % 13 == pokerhand[1] % 13){
       counter++;
     }
     else if (pokerhand[0] % 13 == pokerhand[2] % 13){
       counter++;
     }
     else if(pokerhand[0] % 13 == pokerhand[3] % 13){
       counter++;
     }
     else if(pokerhand[0] % 13 == pokerhand[4] % 13){
       counter++;
     }
     else if(pokerhand[1] % 13 == pokerhand[2] % 13){
       counter++;
     }
     else if (pokerhand[1] % 13 == pokerhand[3] % 13){
       counter++;
     }
     else if(pokerhand[1] % 13 == pokerhand[4] % 13){
       counter++;
     }
     else if(pokerhand[2] % 13 == pokerhand[3] % 13){
       counter++;
     }
     else if(pokerhand[2] % 13 == pokerhand[4] % 13){
       counter++;
     }
     else if(pokerhand[3] % 13 == pokerhand[4] % 13){
       counter++;
     }
     else{
       counter = 0;//there are no pairs
     }    
     if (counter > 0){
       pairBoolean = true; // there are pairs
     }
     else{
       pairBoolean = false;
     }
     return pairBoolean; // return boolean 
  } // end of pair method

  
  public static boolean three(int[]pokerhand){ //initialize three of a kind method
    boolean threeBoolean = false; //boolean for three of a kind
    int counter = 0; // initialize counter
    int x = 2;
    int y = pokerhand[0] % 13;
    int z = pokerhand[1] % 13;
    for (int a = pokerhand[x]%13; x < 5 ; x++){
      if (y == z && z == a){
        counter++;
      }
    }
    x = 3;
    y = pokerhand[1]%13;
    z = pokerhand[2]%13;
    for (int b = pokerhand[x]%13; x < 5; x++){
      if(y == z && z == b){
        counter++;
      }
    }
    y = pokerhand[2]%13;
    z = pokerhand[3]%13;
    int c = pokerhand[4]%13;
    
    if (y == z && z == c){
      counter++;
    }
    y = pokerhand[3] % 13; 
    z = pokerhand[0] % 13; 
    int e = pokerhand[4] % 13; 
    
    if (y == z && z == e){
      counter++;
    }
    
    y = pokerhand[4] % 13; 
    z = pokerhand[0] % 13; 
    int g = pokerhand[2] % 13; 
    
    if (y == z && z == g){
      counter++;
    }
    
    y = pokerhand[3]%13;
    z = pokerhand[0]%13;
    int d = pokerhand[2]%13;
    
    if(y == z && z == d){
      counter++;
    }
   
    y = pokerhand[3] % 13; 
    z = pokerhand[1] % 13; 
    int f = pokerhand[4] % 13; 
    
    if (y == z && z == f){
      counter++;
    }
   
    if (counter > 0){
      threeBoolean = true; //there is a three of akind
    }
    else{
      threeBoolean = false; //there is not a three of a kind
    }
    return threeBoolean; //return three of a kind 
  }
  
  public static boolean flush(int[]pokerhand){ // initialize flush method
    boolean flushBoolean = false;
    if (pokerhand[0]/13== pokerhand[1] /13){ // if card is of same suit
      if (pokerhand[1]/13 == pokerhand[2]/13){ // of same suit
        if(pokerhand[2]/13 == pokerhand[3]/13){ // of same suit
          if(pokerhand[3]/13 == pokerhand[4]/13){ // of same suit
            flushBoolean = true; // flush
          }
        }
      }
    }
    else{
      flushBoolean = false; //no flushes
    }
    return flushBoolean; // return boolean 
  }
  
  public static boolean fullhouse(int[]pokerhand){ //initialize full house method
      boolean fullhouseBoolean = false; //initialize fullhouseBoolean
      
      // full house is when three matching cards of one rank and two matching cards of another rank
    
      if(pokerhand[0] % 13 == pokerhand[1] % 13){ 
        if (pokerhand[1] % 13 == pokerhand[2] % 13){
            if(pokerhand[3] % 13 == pokerhand [4] % 13){
              fullhouseBoolean = true;
            }
          }
        }
    
    if(pokerhand[0] % 13 == pokerhand[1] % 13){
      if(pokerhand[1] % 13 == pokerhand[3] % 13){
        if(pokerhand[2] % 13 == pokerhand [4] % 13){
          fullhouseBoolean = true;
        }
      }
    }
     if(pokerhand[0] % 13 == pokerhand[1] % 13){
      if(pokerhand[1] % 13 == pokerhand[4] % 13){
        if(pokerhand[2] % 13 == pokerhand [3] % 13){
          fullhouseBoolean = true;
        }
      }
    }
     if(pokerhand[1] % 13 == pokerhand[2] % 13){
      if(pokerhand[2] % 13 == pokerhand[3] % 13){
        if(pokerhand[0] % 13 == pokerhand [4] % 13){
          fullhouseBoolean = true;
        }
      }
    }
     if(pokerhand[1] % 13 == pokerhand[2] % 13){
      if(pokerhand[2] % 13 == pokerhand[4] % 13){
        if(pokerhand[0] % 13 == pokerhand [3] % 13){
          fullhouseBoolean = true;
        }
      }
    }
     if(pokerhand[2] % 13 == pokerhand[3] % 13){
      if(pokerhand[3] % 13 == pokerhand[4] % 13){
        if(pokerhand[3] % 13 == pokerhand [4] % 13){
          fullhouseBoolean = true;
        }
      }
    }
     if(pokerhand[3] % 13 == pokerhand[0] % 13){
      if(pokerhand[0] % 13 == pokerhand[4] % 13){
        if(pokerhand[1] % 13 == pokerhand [2] % 13){
          fullhouseBoolean = true;
        }
      }
    }
     if(pokerhand[3] % 13 == pokerhand[1] % 13){
      if(pokerhand[1] % 13 == pokerhand[4] % 13){
        if(pokerhand[0] % 13 == pokerhand [2] % 13){
          fullhouseBoolean = true;
        }
      }
    }
     if(pokerhand[4] % 13 == pokerhand[0] % 13){
      if(pokerhand[0] % 13 == pokerhand[2] % 13){
        if(pokerhand[1] % 13 == pokerhand [3] % 13){
          fullhouseBoolean = true;
        }
      }
    }
    else{
      fullhouseBoolean = false;
    }
    return fullhouseBoolean; // returns full house falsse
  } // end of fullhouse method
  
  public static int highcardhand(int[]pokerhand){ // initalize highcard hand method
       
    int[] sort = new int[]{pokerhand[0] % 13, pokerhand[1] % 13, pokerhand[2] % 13, pokerhand[3] % 13, pokerhand[4] % 13};
    Arrays.sort(sort); //sort hand
    int highest = sort[sort.length - 1];
    
    return highest; // return highest card length
    
  } //method to find the highest rank in a hand

  }//end of class