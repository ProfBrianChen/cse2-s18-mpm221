/////////////////
///CSE 2 Lab06
///Encrypted_x
//Maddie Monahan
//3.9.18
//
import java.util.Scanner; //import scanner
public class encrypted_x{  // initialize class
  public static void main (String[]args){ //initialize main method
  Scanner myScanner = new Scanner (System.in); //initialize scanner
  int starQuantity = 101;//set first quantity to one more than total value possile
  while(starQuantity > 100 || starQuantity < 0){//start while loop restricting domain
    System.out.print("enter a # 1-100: "); //ask for input from user
      if (myScanner.hasNextInt()==true){//if this statement is true
        starQuantity = myScanner.nextInt();//set the quantity to the scanner int
        }
        else{
          String incorrectInput = myScanner.next();//release faulty info, try again  
        }
  }//end of while loop
  
  starQuantity+=1; //adds one to the total quanity in order to make pattern
    
  for (int i = 1; i < starQuantity; i++){//starts for loop for first half of x
    for (int j = 1; j < starQuantity; j++){//initializes other for loop
      if(j==i){
        System.out.print(" ");//print whitespace
      }//end of if 
      else if(j == (starQuantity-i)){
        System.out.print(" ");//print whitespace
      }//end of else if
      else{
        System.out.print("*");//print the stars
      }//end of else
      }//end of first for loop
      System.out.print('\n');//new line
    }//end of second for loop
   }//end of main method
}//end of class
              