/////////////////
/// CSE 2 Lab06
/// Twist Generator
// Maddie Monahan
// 3.2.18
//
import java.util.Scanner; //import scanner

public class TwistGenerator{  // initialize class
  public static void main (String[]args){ //initialize main method
    
  Scanner myScanner = new Scanner (System.in); //name scanner 
   
  int length = 0; //set length variable to 0 before running program
  System.out.print("Enter length: "); //ask for input of length for program
    while (true){ //initialize while loop to run forever, as true 
      if (myScanner.hasNextInt()){ //if scanner input is an an int
        length = myScanner.nextInt();
        if (length<0){
          continue;
        }
        break;
      }
      else{
        String deleteval = myScanner.next();
        System.out.print("invalid input, try again. enter length: ");
      }
    }
    int full = (length / 3);
    int part = (length % 3);
    int count = 1;
    //first row calculations for output to be printed
    while (count <= full){
      System.out.print("\\ /");
      count++;
    }
    if (part == 0){ //only one symbol will be printed if remainder is 0
      System.out.print("\n");
      count = 1;
    }
    else if (part == 1){
      System.out.print("\\");
      System.out.print("\n");
      count = 1;
    }
    else{
      System.out.print("\\ ");
      System.out.print("\n");
      count = 1;
    }
    
    //second row 
    while (count <= full){
      System.out.print(" X ");
      count++;
    }
    if (part == 0){
      System.out.print("\n");
      count = 1;
    }
    else if (part == 1){
      System.out.print(" ");
      System.out.print("\n");
      count = 1;
    }
    else {
      System.out.print(" X");
      System.out.print("\n");
      count = 1;
    }
    
    //third row
    while (count <= full){
      System.out.print("/ \\");
      count++;
    }
    if (part == 0){
      System.out.print("\n");
      count = 1;
    }
    else if (part == 1){
      System.out.print("/");
      System.out.print("\n");
      count = 1;
    }
    else{
      System.out.print("/ \\");
      System.out.print("\n");
      count = 1;
    }
        
    }
    
  }



