/////////////////
/// CSE 2 HW06
/// HW06
// Maddie Monahan
// 2.18.18
//
import java.util.Scanner; //imports Scanner to ask for data in program

//introduce the specific class for lab, should match the name of program
public class Hw05{
  
    		//main method required for every Java program
   			public static void main(String[] args) {
        
        //sets up boolean types for each constraint to be tested
        boolean correctCourse;
        boolean departName;
        boolean numberTimes;
        boolean classTime;
        boolean instructName;
        boolean noStudents;
          
        //course number test
        Scanner myScanner = new Scanner (System.in); //sets up scanner for first tests
        do{ //initializes do,while loop to test course number
          System.out.print("Enter course number: ");
          if (myScanner.hasNextInt()){ //if input is an integer
            correctCourse = true; //set boolean to true
            int coursenumber = myScanner.nextInt();
          }
          else{ //if input is not integer
            System.out.println("Invalid course number, input must be of integer type.");
            correctCourse = false; //bool is false
            myScanner.next(); //clear input
          }
        }while (!(correctCourse)); //while correct course s not true
          
        //department name 
        Scanner myScanner2 = new Scanner (System.in); //sets up scanner for second test
        do{ //initializes do, while loop to test dept name
          System.out.print("Enter department name: ");
          if (myScanner2.hasNext()){ //if input is a string
            departName = true; //bool is true
            String departmentname = myScanner2.next();
          }
          else{ //if input is not a string
            System.out.println("Invalid name, input must be of string type.");
            departName = false; //bool is false
            myScanner2.next(); //clear input and repeat
          }
        }while (!(departName)); //while department name boolean is not true
        
         
        //rest of inputs are exactly the same set up
          
        //number of times a week
         Scanner myScanner3 = new Scanner (System.in);
         do{
          System.out.print("Enter how many times class meets a week: ");
          if (myScanner3.hasNextInt()){
            numberTimes = true;
            int numberclasstimes = myScanner3.nextInt();
          }
          else{
            System.out.println("Invalid number of times, input must be of integer type.");
            numberTimes = false;
            myScanner3.next();
          }
        }while (!(numberTimes));
          
          
        //time class starts
        Scanner myScanner4 = new Scanner (System.in);
         do{
         System.out.print("Enter time of class: ");
          if (myScanner4.hasNextInt()){
            classTime = true;
            int classhours = myScanner4.nextInt();
          }
          else{
            System.out.println("Invalid time, input must be of integer type.");
            classTime = false;
            myScanner4.next();
          }
        }while (!(classTime));
          
          
        //instructor name
        Scanner myScanner5 = new Scanner (System.in);
        do{
          System.out.print("Enter instructor name: ");
          if (myScanner5.hasNext()){
            instructName = true;
            String instructorName = myScanner5.next();
          }
          else{
            System.out.println("Invalid name, input must be of string type.");
            instructName = false;
            myScanner5.next();
          }
        }while (!(instructName));
          
          
        //number of students
        Scanner myScanner6 = new Scanner (System.in);
         do{
         System.out.print("Enter number of students in class: ");
          if (myScanner6.hasNextInt()){
            noStudents = true;
            int numberofstudents = myScanner6.nextInt();
          }
          else{
            System.out.println("Invalid number, input must be of integer type.");
            noStudents = false;
            myScanner6.next();
          }
        }while (!(noStudents));
        
    
          
        
          
      }//end of m.m.
    }//end of class