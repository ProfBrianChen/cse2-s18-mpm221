/////////////////
///CSE 2 Lab02
///Cyclometer
//Records data from cyclometer and prints minutes, counts, and distance for trip
//Maddie Monahan
//2.2.18
//
public class Cyclometer {
    // main method required for every Java program
   	public static void main(String[] args) {
     
    // input data from bicylcle trips 
    int secsTrip1=480; // duration, in seconds, of trip 1
    int secsTrip2=3220; // duration, in seconds, of trip 1
    int countsTrip1=1561; // cycles of wheel during trip 1
    int countsTrip2=9037; // cycles of wheel during trip 2
      
    // intermediate variables and output data for calculations of trip
    double wheelDiameter=27.0,  // diameter of bicycle wheel used in trips held in a double variable
  	PI=3.14159, // constant to be used in calculation of 'counts'
  	feetPerMile=5280,  // constant to be used in calcuation of distance 
  	inchesPerFoot=12,   // constant to be used in calculation for distance
  	secondsPerMinute=60;  // constant to be used for calculation of time
    double distanceTrip1, distanceTrip2,totalDistance;  // casting these three variables into double to compute data
     
    // prints numbers stored in the variables for minutes and cycles for trip 1
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
    
    // prints numbers stored in the variables for minutes and cycles for trip 2
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
    
    // running calculations and storing values
    // below equation calculates distance (in inches)
    // calculation based upon fact that bike travels diameter of wheeel * pi every count
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
    
    // below calculates the same equation for trip2
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
    
    // Print out the output data of distance per trip
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    
    // Prints the output, combining data from trips 1 and 2
    System.out.println("The total distance was "+totalDistance+" miles");

	}  
    //end of main method   
} 
//end of class
