// Maddie Monahan
// HW10
// Robot City

import java.util.Scanner; // import scanner
import java.util.Arrays; // imports Array class
import java.util.Random; // imports Random

public class RobotCity{ // initializes class
  
  
public static int [][]buildCity(){ //initialize build city array
    // creating array from random length 10 to 15
    int [][] array = new int [(int)(Math.random()*((5)+1)+10)][(int)(Math.random()*((5)+1)+10)];
    // repeat rby length of array
    for (int x = 0; x < array.length ; x++){
      for (int y = 0; y < array[x].length; y++){
        // initializing each member from 100 to 999
        array[x][y] =  (int)(Math.random() * ((999-100)+1) + 100);
      } // end of for loop
    } // end of second for loop
    return array;   // display array                                                    
} // end of build city method
  
                            
                            
public static int [][]invade(int[][]array, int k){ // initialize invade method
    for (int j = 0; j < k; j++){ // repeat for k times
      int x = (int)(Math.random() * (array.length)); // rows
      int y = (int)(Math.random() * (array[0].length)); // columns
      array[x][y] = (-1)*array[x][y];
    } // end of loop
    return array;
 } // end of invade method
  
public static void update (int[][]array){ //initialize update method
    for (int x = 0; x < array.length; x++){ // for loop for rows
      for (int y = 0; y < array[x].length; y++){ // columns
        if (array[x][y]<0){ // if negative
          array[x][y] = array[x][y] * (-1); 
        if (y < array[x].length - 1){ // if colums less
          array[x][++y] = array[x][y] * (-1);
        } //end this if
        } // end of large if
      } // end of columns for loop
    } // end of rows
} // end of update method
  
public static void main(String [] arg){ //initializes main method
  
  // generate random number
 Random randomGenerator = new Random();
  // between 1 and 10
 int no = randomGenerator.nextInt(10);
 // draw city array 
  int draw[][] = buildCity();
 // display draw 2 dimensional array
  display(draw);
 // display number of robots invading
  System.out.println(no + "   robots invading");
  System.out.println("");
  // invade with number of robots
  invade(draw, no);
  // display array
  display(draw);
  
   // repeat 5 times
    for (int i = 1; i < 6; i++){
      System.out.println("");
      System.out.println("Attempt  " + i + ":");
      System.out.println("");
      // update array
      update(draw);
      // display array
      display(draw);
    } // end of for loop
 }// end of main method
  
public static void display(int[][]array){ // display method
  for (int x = 0; x<array.length; x++){ // row loop
    for (int y = 0; y < array[x].length; y++){ //column loop
      System.out.printf(" %d ", array[x][y]); // print with printf arrray population
    } // end of column loop
  System.out.println(" "); // print lines
  System.out.println(" ");
  } // end of row loop 
}// end of display method
  
  
 } // end of class
                       