/////////////////
///CSE 2 Lab03
///Check
//Computes how much party owes in dinner based upon bill amount and desired tip
//Maddie Monahan
//2.9.18
//
import java.util.Scanner; //imports Scanner to ask for data in program
//introduce the specific class for lab, should match the name of program
public class Check{
    		// main method required for every Java program
   			public static void main(String[] args) {
          
          
          
        Scanner myScanner = new Scanner (System.in); //constructs instance of Scanner to take input from STDIN
        System.out.print("Enter the original cost of the check in the form xx.xx: "); //ask for input of total bill amount 
        double checkCost = myScanner.nextDouble(); //accept user input of check cost from scanner and declare a double
        System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //ask for percentage tip
        double tipPercent = myScanner.nextDouble (); //accept user input of tip percentage as whole number from scanner and declare as a double
        tipPercent /= 100; //Conversion of percentage to decimal 
        System.out.print("Enter the number of people who went out to dinner: "); //ask for input of number of people who went out to dinner
        int numPeople = myScanner.nextInt(); //accept input with scanner of number of people bill should be split with and declare as an int
        double totalCost; //declare totalCost, including tip, as a double, for use at end
        double costPerPerson; //declare cost per person as a double variable for use at end of program to print final result
        int dollars,   //whole dollar amount of cost 
          dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
        totalCost = checkCost * (1 + tipPercent); //total cost of bill is equal to the multiplication of check cost and the sum of 1+tip percent. This equation will compute data at end of program.
        costPerPerson = totalCost / numPeople; //divides total Cost to see the cost per person
        //get the whole amount, dropping decimal fraction
        dollars=(int)costPerPerson; //cast costPerPerson as an integer and gets rid of decimal 
        //get dimes amount, e.g., 
        // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
        //  where the % (mod) operator returns the remainder
        //  after the division:   583%100 -> 83, 27%5 -> 2 
        dimes=(int)(costPerPerson * 10) % 10; //cast costperperson as an int, multiplied to a whole number, to return remainder
        pennies=(int)(costPerPerson * 100) % 10; //casts costperperson as an int, multiplied by 100 because 100 pennies are in a dollar, to return remainder using mod function
        System.out.println("Each person in the group owes $" +dollars + '.' + dimes + pennies); //print final data, '.' is necessary for scanner data 

         }  //end of main method   
  	} //end of class
