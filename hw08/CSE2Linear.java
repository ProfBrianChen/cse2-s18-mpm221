// CSE2Linear
/// Maddie Monahan
//// Homework 8
///// Program 1
///
//
import java.util.Scanner; // import scanner 

public class CSE2Linear{ // initializes class
 
  public static void main(String[] args){ // starts main method
  Scanner myScanner = new Scanner(System.in); // initializes scanner 
  System.out.print("Enter 15 ints in ascending order: "); // asks for inputs
  int input = 0; // sets counter to 0
  int[]grades = new int[15]; // initializes new array 
  while (input<15){ // starts while loop for each data point entry
    if(myScanner.hasNextInt()){ // ensuring scanner has int
      grades[input] = myScanner.nextInt(); // if right, scanner in next array
      if(grades[input] > 100 || grades[input] < 0){ // ensuring data input is between 0 and 100
        System.out.println("Range is between 0 and 100.");
        System.exit(1); // if not, exit program
      }
      {
        if(input > 0){ // make sure values are greater than previous input 
          if (grades[input-1] > grades[input]){
            System.out.println("This is not greater than the last input. ");
            System.exit(1); // if incorrect, exit code
          }
        }
        input++; // counter increase 
      }
    }
    else{
      System.out.println("Not an integer."); // not an integer, exit 
      System.exit(1);
    }
  }
    System.out.println("Here are your choices to choose from:"); // ask for what to choose from for binary
    String character = "[";
     for (int x=0; x<15; x++){
            character+= " " + grades[x];
        }
    
   System.out.println(" " + character + "]"); // prints out all data in array
   System.out.print("Enter a score to search for: "); // enter a score to search for 
   if(myScanner.hasNextInt()){ // if the data is in the scanner input as an integer
     int data = myScanner.nextInt(); // initalizes data point to the scanner 
     binarysearchfordata(grades, data); // call binary search method
     scramble(grades); // scramble data
     System.out.print("Here's the scrambled data. Choose again for linear search: "); // ask for linear search method
     if(myScanner.hasNextInt()){ // if input for data choice is an integer
     linearsearchfordata(grades, data); // call linear search method
     }
   }
    else{
     System.exit(1);
    }
  } // end of main method
    
    
  public static void linearsearchfordata(int[]grades, int find){ // linear search method

    boolean FoundValueLinear = false; // start boolean for linear found value
    int iterations = 0; // set iterations counter 
    for (int j = 0; j<15&&!FoundValueLinear; j++){ // loop to calculate linear search
      if (find == grades[j]){
        FoundValueLinear = true;
        iterations = j; //how many tries it took to find
      }
    }
    if(FoundValueLinear == true){
        System.out.println("found this number in " + iterations + " iterations.");
      }
      else{
        System.out.println("not found.");
      }
    } // end of linear search
  
  public static void binarysearchfordata(int[]grades, int find2){ // binary search method 
   
    int startingVal = 0; // start
    int endingVal = grades.length-1; // val length of grades minus one 
    int index = (startingVal+endingVal)/2; // middle point of array to start caluclations fpr binary
    boolean FoundDataPoint = false; // sets boolean to false
    int iterations = 0; // sets iterations to 0
    // calculations for binary search method
    while (startingVal<=endingVal){
      if(find2>grades[index]){
        startingVal = index+1;
      }
      else if(grades[index]==find2){
        startingVal = endingVal +1;
        FoundDataPoint = true; // if found, boolean is now true
      }
      else{
        endingVal = index-1;
      }
      index = (startingVal+endingVal)/2; // index is in center of value
      iterations++; // count increases
    }
    if (FoundDataPoint == true){
      System.out.println("This grade was found in " +iterations+ " iterations.");
    }
    else{
      System.out.println("Not found!");
    }
    } // end of binary search
  
  public static void scramble(int []grades){ // scramble method
    for (int i = 0; i<15; i++){ // for loop to scramble array
      int random = (int)(Math.random()*15); // picks random number to scramble each point
      int temp = grades[i]; // sets grades to temporary array
      grades[i]= grades[random]; // sets array to random array
      grades[random]= temp; // brings random back to temp, finalizing randoming of array
    }
    String string = "["; // initialize print out of random
    for (int j = 0; j < 15; j++){ // for loop to print random string
      string+=" " + grades[j];
    }
     System.out.println(string+"]");
} // end of scramble method
  
} // end of class
