// CSE2Linear
/// Maddie Monahan
//// Homework 8
///// Program 2
///
//
import java.util.Scanner; // import scanner 

public class RemoveElements{ // initializes class
  
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.println("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  } // end main method
 
  public static String listArray(int []num){ //initialize method for list array
	String out="{";  // starting part of output
	for(int j=1;j<num.length;j++){ // for loop, number '1' is position in first array
  	if(j>1){ // if j is greater than one, remove the num in that position
    	out+=", "; // insert comma
  	}
  	out+=num[j];
	}
	out+="} "; //end of string
	return out; // return the outputted array
  }//end list array method

 
public static int[] randomInput(){ //initializes method for random input
  int randNumber[]= new int[10]; // initializes array for random numbers
  for (int j = 0; j < randNumber.length; j++){ // loop to get array 
    int arrayPrint = (int)(Math.random()*10);
    randNumber[j] = arrayPrint; // places random number in array
  } // end of for loop
  return randNumber; // returns random number array
} // end of method

public static int[] delete(int[]list, int pos){ //intialize delete method
  if (pos > 10 || pos < 0){ //ensure that data is within limits
    System.out.println("The index is not valid.");
    System.exit(1); // leave program
  }
  int[]deleteFormat = new int [9];  // initialize array of 9 integers
  for (int i = 0; i < pos; i++){ // start integer string to print out
    deleteFormat[i]=list[i]; 
  }
  for (int j = 1+pos; j < list.length; j++){
   deleteFormat[pos] = list[j]; 
   pos++;
  }
  return deleteFormat; // return deleted numbber
  
}// end random input method

public static int[] remove(int[]list, int target){ //remove method initialized
  int counter1 = 0;  // setfirst count to 0
  int counter2 = 0;  // set second count to 0
  
  for(int j = 0; j < list.length; j++){ // set up for loop  
    if(list[j] != target){ //if target is in array
      counter1++; // increment counter 
    }// end of if statement 
  } // end for loop
  int remove[] = new int[counter1]; //remove array initialized
  for (int x = 0; x < list.length; x++){
    if (list[x] != target){ // if target is not in the list
      remove[counter2] = list[x]; // increase counter and try again 
      counter2++;
    }
  } // end for loop
  return remove; // return the removed array
} // end remove method
}// end of class
