/////////////////
///CSE 2 Lab04
///Card Generator
//Maddie Monahan
//2.16.18
//

//introduce the specific class for lab, should match the name of program
public class CardGenerator {
    		// main method required for every Java program
   			public static void main(String[] args) {
        String suit = " "; //ensure suits are casted as strings
        String number = " "; //ensure card # in deck is casted as string 
        int randomNumber = (int)(Math.random()*13)+1; //follows formula of random number and adds 1 to eliminate [0,13] but instead [1,13]
        int randomSuit = (int)(Math.random()*4)+1;
        //begin switch method for number within cards
        switch(randomNumber % 13) {
          case 1:
            number = "ace";
            break;
          case 2:
            number = "2";
            break;
          case 3:
            number = "3";
            break;
          case 4:
            number = "4";
            break;
          case 5:
            number = "5";
            break;
          case 6:
            number = "6";
            break;
          case 7:
            number = "7";
            break;
          case 8:
            number = "8";
            break;
          case 9:
            number = "9";
            break;
          case 10:
            number = "10";
            break;
          case 11:
            number = "Jack";
            break;
          case 12:
            number = "Queen";
            break;
          case 13:
            number = "King";
            break;
        }
      //starts switch method for suits of cards
      switch (randomSuit % 4){
          case 1:
            suit = "clubs";
            break;
          case 2:
            suit = "spades";
            break;
          case 3:
            suit = "hearts";
            break;
          case 4:
            suit = "diamonds";
            break;
        }
          //prints final statement
          System.out.println("You picked the " + number + " of " + suit + ".");
        }//end of main method
}//end of class

          

        